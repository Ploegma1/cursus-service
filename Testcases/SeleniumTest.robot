*** Settings ***
Resource             ../Keywords/SeleniumKeywords.robot
Suite Teardown       Close All Browsers
Documentation        Keyword documentation: https://robotframework.org/SeleniumLibrary/SeleniumLibrary.html
...
...                  Kijk goed of er keywords zijn die je kan gebruiken in de Keywords/SeleniumKeywords.robot file

*** Variable ***
${SELENIUM_DELAY}    1
${TEST_URL}          http://cursusclient-gryffindor-hco-kza-cursus.apps.prod.am5.hotcontainers.nl/cursussen

${CURSUS_NAAM}       Dit Is Een Cursus Naam

*** Test Cases ***

Opdracht 1: Login in de cursusclient
    Open Browser In Jenkins             ${TEST_URL}
    Wait Until Page Contains Element    id=login                  timeout=5
    Click Element                       id=login                  
    Wait Until Page Contains Element    id=logout                 timeout=5

Opdracht 2: Controleer of de pagina correct geladen is
    Page Should Contain                 Cursus kalender
    Page Should Contain                 Inloggen
    Wait Until Page Contains            Inloggen                  timeout=1
    Select From List By Index           id=listID                 0
    Sleep  0.5

Opdracht 3: Open het scherm om een nieuwe training aan te maken
    Click Element                       id=create  
    Page Should Contain                 Cursus aanmaken    

Opdracht 4: Maak een nieuwe training aan
    Vul Veld Cursus                     trainingName               Testtraining


Opdracht 5: Meld je aan voor de training die je net hebt gemaakt

Opdracht 6: Controleer of de aangemaakte training in de lijst van trainingen terugkomt

Opdracht 7: Controleer de details pagina van de aangemaakte training

Opdracht 8: Verwijder de aangemaakte training

Opdracht 9: Test de logout functionaliteit
